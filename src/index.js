import { io } from "socket.io-client"

export function createSocketClient(port, host='localhost'){
    const socket = io(`http://${host}:${port}`)
    return socket
}

export function registerSocketEventHandler(socket, eventName, callback){
    socket.on(eventName, callback)
}

export function emit(socket, eventName, data){
    socket.emit(eventName, data)
}

export function getCommsConfig (){
    let host
    let socketServerPort
    let fileServerPort
    try {
        let urlParams = new URLSearchParams(window.location.search)
        host = urlParams.get('host') || 'localhost'
        socketServerPort = urlParams.get('socketServerPort') || 0
        fileServerPort = urlParams.get('fileServerPort') || 0
    } catch (error) {
        console.error(error)
    }
    return {
        host: host,
        socketServerPort: socketServerPort, 
        fileServerPort: fileServerPort
    }
}

export function createCommandString(options={}, defaults={}, callback=()=>{}){
    let command = ''
    for (const [key, value] of Object.entries(options)) {
        if (value === null){
            continue
        } else if (value !== defaults[key]){
            // only add the option if it is different from the default
            if (value === true){
                // just pass the flag to indicate true
                command += ` ${key}`
            } else {
                // dont add the key if key does not start with a dash
                if (key[0] !== '-'){
                    command += ` ${value}`
                } else {
                    command += ` ${key} ${value}`
                }
            }
        } else if (value === true){
            // just pass the flag to indicate true
            command += ` ${key}`
        }
    }
    callback(command)
    return command
}

export function removeExtension(str, ext='.nii'){
    // split the string into an array
    let arr = str.split(ext)
    // remove the last element of the array
    arr.pop()
    // join the array back into a string
    return arr.join('')
}

export function getFilenameFromURL(url){
    let filename = url.split('/').pop()
    return filename
}

export function webGl2Supported(){
    let canvas = document.createElement('canvas')
    let gl = canvas.getContext('webgl2')
    return gl !== null
}



